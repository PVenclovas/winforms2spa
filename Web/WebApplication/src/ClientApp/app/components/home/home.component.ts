import { Component } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {
    private connection: HubConnection;
    constructor() {
        this.initializeCommunications();
    }
    ngOnInit() {
        
    }

    initializeCommunications() {
        this.connection = new HubConnectionBuilder()
            .withUrl("http://localhost:62205/ChatHub")
            .build();

        this.connection.on("ReceiveMessage",
            (username: string, message: string) => {
                let m = document.createElement("div");
                let console: HTMLDivElement = document.getElementById('console') as HTMLDivElement;
                m.innerHTML =
                    `<div class="message__author">${username}</div><div>${message}</div>`;
                console.appendChild(m);
            });

        this.connection
            .start()
            .then(() => {
                console.log("connection started");

            })
            .catch(err => console.log("Connection failed", err));


    }
    onPublishEvent() {
        this.connection.invoke("SendMessage", "Paulius", "Hi");
    }
}
