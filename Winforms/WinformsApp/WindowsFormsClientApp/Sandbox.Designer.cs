﻿namespace WindowsFormsClientApp
{
    partial class Sandbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Console = new System.Windows.Forms.TextBox();
            this.publishEventButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Console
            // 
            this.Console.Location = new System.Drawing.Point(13, 13);
            this.Console.Multiline = true;
            this.Console.Name = "Console";
            this.Console.Size = new System.Drawing.Size(599, 387);
            this.Console.TabIndex = 0;
            // 
            // publishEventButton
            // 
            this.publishEventButton.Enabled = false;
            this.publishEventButton.Location = new System.Drawing.Point(509, 406);
            this.publishEventButton.Name = "publishEventButton";
            this.publishEventButton.Size = new System.Drawing.Size(103, 23);
            this.publishEventButton.TabIndex = 1;
            this.publishEventButton.Text = "Publish event";
            this.publishEventButton.UseVisualStyleBackColor = true;
            this.publishEventButton.Click += new System.EventHandler(this.PublishEvent_Click);
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(428, 406);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // Sandbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.publishEventButton);
            this.Controls.Add(this.Console);
            this.MinimumSize = new System.Drawing.Size(320, 240);
            this.Name = "Sandbox";
            this.Text = "Sandbox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Console;
        private System.Windows.Forms.Button publishEventButton;
        private System.Windows.Forms.Button connectButton;
    }
}

