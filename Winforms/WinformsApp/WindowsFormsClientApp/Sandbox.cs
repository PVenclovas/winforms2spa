﻿using System;
using System.Windows.Forms;
using Microsoft.AspNetCore.SignalR.Client;

namespace WindowsFormsClientApp
{
    public partial class Sandbox : Form
    {
        private HubConnection _hubConnection;
        public Sandbox()
        {
            InitializeComponent();
            InitializeCommunications();
        }

        private async void PublishEvent_Click(object sender, EventArgs e)
        {
            await _hubConnection.InvokeAsync("SendMessage",
                "Venclovas", "Hello");
        }
        void InitializeCommunications()
        {
            _hubConnection = new HubConnectionBuilder().WithUrl("http://localhost:62205/ChatHub").Build();
        }

        private async void ConnectButton_Click(object sender, EventArgs e)
        {
            _hubConnection.On<string, string>("ReceiveMessage", (user, message) =>
            {
                if (this.Console.InvokeRequired)
                    Console.Invoke(new MethodInvoker(() => Console.AppendText($"{user}: {message}\n")));
            });

            try
            {
                await _hubConnection.StartAsync();
                this.Console.AppendText("Connection started\n");
                connectButton.Enabled = false;
                publishEventButton.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Console.Text += ex.Message;
            }
        }
    }
}
